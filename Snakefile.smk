# Snakemake workflow example.
#
# Snakemake feature: fastq from csv file, config file in yaml, modules (rules), SLURM (json), report


import pandas as pd
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")

#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_table(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

#List unique values in the samples['Group'] column
groups = samples.Group.unique() #list of uniq group name
# or:
# groups = samples['Group'].unique()

##### load rules #####
include: "modules/utils.smk"
include: "modules/PREPROC_fastp_pe.smk"
include: "modules/QC_fastqc.smk"
include: "modules/multiqc_pe.smk"
include: "modules/MAPPING_bwa_pe.smk"
include: "modules/BAM_bamslist.smk"

rule all:
    input:
        #expand("{outdir}/fastp/{sample}_1_trim.fastq.gz", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/fastp/{sample}_2_trim.fastq.gz", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName']),
        expand("{outdir}/multiqc/multiqc_report.html", outdir=config["outdir"]),
        expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName']),
        "{outdir}/bams.list".format(outdir=config["outdir"])
        #expand("{outdir}/mapped/{group}_sorted.bam", outdir=config["outdir"], group=groups)


