__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

# fastp, single end mode :
#
#   This module run fastp (https://doi.org/10.1093/bioinformatics/bty560)
#   on your single end raw fastq files.
#   fastp perfomrs:
#       - adapters dectection and trimming
#       - low quality bases trimming on both 3' and 5' ends (Qscore < 20)
#       - low complexity regions elimination (ex: polyA tails)
#       - QC report generation in .html format
#
#   Input:
#       - (gz) fastq read
#
#   Output:
#       - {sample}_1_trim.fastq.gz
#       - optionally output: json and html
#
#   Parameters:
#       fastp default parameters (see fastp manual for more information).
#       Thoses parameters all well suited for most of sequencing data,
#       However you can modify them directly into the script bellow if you
#       feel the need to (for advanced users).
#module 4 fastp for Single-end read
#use with utils.smk
# 2alternatives preprocessing

rule fastp_se:
    input:
        R1  = get_fastq1,
        #R1 = lambda wildcards: "../data/"+samples.fq1[wildcards.sample],
        #R1  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna(),
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
    message: "Running fastp on files {input.R1} \n"
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        modules    = config["MODULES"],
        fastp_bin  = config["fastp_bin"],
        bind       = config["BIND"],
        json       = config["outdir"]+"/fastp/{sample}_trim.json",
        html       = config["outdir"]+"/fastp/{sample}_trim.html"
    threads: 2
    shell:
       """
        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -o {output.R1} \
        -h {params.html} \
        -j {params.json} \
        --max_len1 350 \
        --correction \
        --cut_mean_quality 20 \
        --cut_window_size 4 \
        --low_complexity_filter \
        --complexity_threshold 30 \
        -w {threads}
        rm -f {params.html} {params.json}
        exit 0

        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -o {output.R1} \
        --json={params.json} \
        --html={params.html} \
        --trim_tail1=1 \
        --cut_front \
        --cut_tail \
        --length_required 35 \
        --average_qual 20 \
        --length_limit 400 \
        --correction \
        --cut_mean_quality 10 \
        --low_complexity_filter \
        --complexity_threshold 20 \
        --thread {threads}
        #rm -f {params.html} {params.json}
        """
