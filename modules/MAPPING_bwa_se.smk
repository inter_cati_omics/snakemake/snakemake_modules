__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

# BWA single end mode:
#
#   Parameters:
# 

#mapping with bwa PE mode
#reference indexing
#example with and without mark duplicate
#sam 2 bam sorted and statistics


# ############################################################
#   Input:
#       - config["REFPATH"]/config["GENOME"]
#   Output:
#       - config["REFPATH"]/reference_indexed
#   Requirement:
#       - none
# ############################################################

rule bwa_index:
    input:
        genome = config["REFPATH"] + "/" + config["GENOME"]
    output:
        config["REFPATH"] + "/" + config["GENOME"] + ".amb",
        config["REFPATH"] + "/" + config["GENOME"] + ".ann",
        config["REFPATH"] + "/" + config["GENOME"] + ".bwt",
        config["REFPATH"] + "/" + config["GENOME"] + ".pac",
        config["REFPATH"] + "/" + config["GENOME"] + ".sa",
        config["REFPATH"] + "/" + config["GENOME"] + ".fai"
    params:
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        samtools_bin = config["samtools_bin"]
    message: "Building BWA index for reference genome {input.genome}\n"
    shell:
        """
        singularity exec {params.bind} {params.bwa_bin} bwa index -a bwtsw -b 500000000 {input.genome}
        singularity exec {params.bind} {params.samtools_bin} samtools faidx {input.genome}
        """
# ############################################################
#   Input:
#       - config["REFPATH"] and config["GENOME"]
#   Output:
#       - config["outdir"]/mapped/{sample}_sorted.bam
#   Requirement:
#       - config["REFPATH"]/config["GENOME"] indexed
#       - trimmed reads in config["outdir"]/fastp/
# ############################################################
rule bwa_se :
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        #fake input used to force index building before alignement if not present
        idx = config["REFPATH"] + "/" + config["GENOME"] + ".bwt"
    output:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        samtools_bin = config["samtools_bin"],
        freebayes_bin= config["freebayes_bin"], # freebayes_bin used for sambamba for markdup
        rg           = "@RG\\tID:{sample}\\tSM:{sample}"
    threads: 10
    message: "Mapping reads {input.R1} to {params.idxbase} using BWA.\n"
    #converting to bam, sorting and removing dupplicates in a single command!
    shell:
        """
        singularity exec {params.bind} {params.bwa_bin} bwa mem \
        -t {threads} \
        -K 100000000 \
        {params.idxbase} \
        {input.R1} \
        | singularity exec {params.bind} {params.samtools_bin} samtools sort -@2 -m 6G -o {params.outdir}/{wildcards.sample}.bam.raw -

        #Without rm duplicate
        singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {params.outdir}/{wildcards.sample}.bam.raw
        mv {params.outdir}/{wildcards.sample}.bam.raw.bai {output.bam}.bai
        mv {params.outdir}/{wildcards.sample}.bam.raw {output.bam}

        #with rm duplicate
        #singularity exec {params.bind} {params.samtools_bin} samtools rmdup -s {params.outdir}/{wildcards.sample}.bam.raw {output.bam}
        #singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {output.bam}

        #singularity exec {params.bind} {params.freebayes_bin} sambamba markdup -t 10 {params.outdir}/{wildcards.sample}.bam.raw {output.bam}
        #singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools flagstat -@ 10 {output.bam}
        rm -f {params.outdir}/{wildcards.sample}.bam.raw*
        """

rule bam_stats:
    input:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
    output:
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        samtools_bin = config["samtools_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} samtools stats -@ 2 {input.bam} > {output.stats}
        """
