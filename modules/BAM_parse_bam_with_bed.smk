__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#Extract regions from a bam using bed file
rule parse_bam_with_bed:
    input:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
    output:
        bam   = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"])
    params:
        bed = config["REGIONS"],
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"]
    message: "Parsing {input.bam} using the blueprint {params.bed} \n"
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} samtools view \
        -b \
        -L {params.bed} \
        -O BAM \
        -o {output.bam} {input.bam}
        """
