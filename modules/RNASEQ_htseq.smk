__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#RNAseq
#gene count level
rule htseq:
        input:
            bam = config["wd"] + "/starpass2/{sample}/Aligned.sortedByCoord.out.bam",
            gff = config["gff3"]
        output:
            out = config["wd"] + "/htseq/{sample}_HTSeq_union.out",
            csv = config["wd"] + "/htseq/{sample}_HTSeq.csv"
        threads: 1
        shell:
            """
            module load system/singularity-2.5.1
            singularity exec -B '/work/jlagnel,/home/jlagnel/work,/nosave/project/gafl,/work/project/gafl' /nosave/project/gafl/tools/containers/htseq.simg htseq-count \
            -m union -s yes -t gene -i ID -r pos -f bam {input.bam} {input.gff} > {output.out}
            grep '^Prupe' {output.out} >{output.csv}
            """
