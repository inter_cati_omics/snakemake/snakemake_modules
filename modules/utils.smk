__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#common utilities
#read CSV file with samples
# get fastq R1, R2 or both
import pandas as pd

#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

#common utilities
#---------------- functions to get fastq files ------------------------------------------
#function return fastq1 and fastq2 files and adds the fastq path
# works with SE, PE or mix of PE+SE fastq
def get_fastq(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()

#function return only the fastq1 file and adds the fastq path
def get_fastq1(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna()

#function return only the fastq2 file and adds the fastq path
def get_fastq2(wildcards):
    return config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()

#get sample name
#def get_sample(wildcards):
#    return samples.loc[(wildcards.sample), ["SampleName"]].dropna()

# get samples list from (uniq values) groups column: 'Group'
def getit(gr):
    df=samples.loc[samples['Group'] == gr,["SampleName"]]
    #df=df[["SampleName"]]
    #convert it to a list
    tt=df["SampleName"].values.tolist()
    #print(tt)
    return tt

#---------------------------------------------------------------------------------------
