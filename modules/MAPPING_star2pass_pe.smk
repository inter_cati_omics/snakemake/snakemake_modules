__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#Rules for STAR mapper using 2pass without reference re-indexing
#based on:
#https://evodify.com/rna-seq-star-snakemake/
# gff3 must be converted to gtf:
#singularity exec /nosave/project/gafl/tools/containers/cufflinks_v2.2.1.sif gffread Annuum.v.2.0.gff3 -T -o Annuum.CM334.v1.6.Total.gtf
rule star_index:
        input:
            genome  = config["REFPATH"] + "/" + config["GENOME"], # provide your reference FASTA file
            gtf = config["REFPATH"] + "/" + config["GTF"]     # provide your GTF file
        output:
            "{refpath}/SAindex".format(refpath=config["REFPATH"]),
            "{refpath}/indexing.OK".format(refpath=config["REFPATH"])
        threads: 20 # set the maximum number of available cores
        params:
            outdir    = config["REFPATH"],
            bind       = config["BIND"],
            star_bin  = config["star_bin"]
        shell:
            """
            cd {params.outdir} && \
            singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
            --runMode genomeGenerate \
            --genomeDir {params.outdir} \
            --genomeFastaFiles {input.genome} \
            --sjdbGTFfile {input.gtf} \
            --sjdbOverhang 100 && \
            touch {output}
            """

rule star_pass1:
        input:
            R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
            R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
            refindex="{refpath}/indexing.OK".format(refpath=config["REFPATH"])
        params:
            bind      = config["BIND"],
            refdir    = config["REFPATH"],
            outdir    = "{outdir}/mapped/starpass1/{{sample}}".format(outdir=config["outdir"]),
            rmbam     = "{outdir}/mapped/starpass1/{{sample}}/Aligned.out.bam".format(outdir=config["outdir"]),
            star_bin  = config["star_bin"]
        output:
            sj      = "{outdir}/mapped/starpass1/{{sample}}/SJ.out.tab".format(outdir=config["outdir"]),
            pass1ok = "{outdir}/mapped/starpass1/{{sample}}/pass1.OK".format(outdir=config["outdir"])
        threads: 10
        shell:
            """
            mkdir -p {params.outdir} && cd {params.outdir} && \
            singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
            --genomeDir {params.refdir} \
            --readFilesIn {input.R1} {input.R2} \
            --readFilesCommand zcat \
            --outSAMtype None && \
            touch {output.pass1ok}
            rm -f {params.rmbam}
            #--outSAMtype BAM Unsorted #if we keep the output bam
            """

# filter and build SJ file for each sample
#rule star_sjdir:
#        output: "{outdir}/mapped/SJ".format(outdir=config["outdir"])
#        threads: 1
#        shell: 'mkdir -p {output}'

rule star_sjfilter:
        input:
            sj       = "{outdir}/mapped/starpass1/{{sample}}/SJ.out.tab".format(outdir=config["outdir"]),
            pass1ok  = "{outdir}/mapped/starpass1/{{sample}}/pass1.OK".format(outdir=config["outdir"]),
            #sjpath   = "{outdir}/mapped/SJ".format(outdir=config["outdir"])
        output:
            sjfiltered  = "{outdir}/mapped/SJ/{{sample}}SJ.filtered.tab".format(outdir=config["outdir"])
        params:
            bind      = config["BIND"],
            star_bin  = config["star_bin"] # to use internal awk,cut and sort
        threads: 1
        shell:
            """
            singularity exec {params.bind} {params.star_bin} cat {input.sj} | \
            singularity exec {params.bind} {params.star_bin} awk '($5 > 0 && $7 > 3 && $6==0)' | \
            singularity exec {params.bind} {params.star_bin} cut -f1-6 | \
            singularity exec {params.bind} {params.star_bin} sort | \
            singularity exec {params.bind} {params.star_bin} uniq > {output.sjfiltered}
            # a simple filter
            #singularity exec {params.bind} {params.star_bin} awk "{{ if (\$7 >= 3) print \$0 }}" {input[0]} > {input[0]}.filtered && \
            #mv {input[0]}.filtered {output.sjfiltered}
            """

# Filter and build one SJ file
rule sj_filter:
    input:
       lambda wildcards: expand(expand("{outdir}/mapped/starpass1/{sample}/SJ.out.tab", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        sjfilmerged = "{outdir}/mapped/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
    params:
        sjpath  = "{outdir}/mapped/starpass1".format(outdir=config["outdir"]),
        sjlist = "{outdir}/mapped/starpass1/*/SJ.out.tab".format(outdir=config["outdir"])
    shell:
        """
        cat {params.sjlist} | \
        singularity exec {params.bind} {params.star_bin} awk '($5 > 0 && $7 > 2 && $6==0)' | \
        singularity exec {params.bind} {params.star_bin} cut -f1-6 | \
        singularity exec {params.bind} {params.star_bin} sort | \
        singularity exec {params.bind} {params.star_bin} uniq > {output}
        """

rule star_pass2:
        input:
            R1        = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
            R2        = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
            refindex  = "{refpath}/indexing.OK".format(refpath=config["REFPATH"]),
            sjs       = lambda wildcards: expand(expand("{outdir}/mapped/SJ/{sample}SJ.filtered.tab", outdir=config["outdir"], sample=samples['SampleName'])),
            #sjfilmerged = "{outdir}/mapped/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
        output:
            bam       = "{outdir}/mapped/starpass2/{{sample}}/Aligned.sortedByCoord.out.bam".format(outdir=config["outdir"]),
            genecount = "{outdir}/mapped/starpass2/{{sample}}/ReadsPerGene.out.tab".format(outdir=config["outdir"]),
            pass2ok   = "{outdir}/mapped/starpass2/{{sample}}/pass2.OK".format(outdir=config["outdir"])
        params:
            refdir       = config["REFPATH"],
            outdir       = "{outdir}/mapped/starpass2/{{sample}}".format(outdir=config["outdir"]),
            id           = "{{sample}}",
            samtools_bin = config["samtools_bin"],
            bind         = config["BIND"],
            star_bin     = config["star_bin"]
        threads: 10 # set the cores
        message:
           "STAR pass 2\n"
        shell:
           """
           mkdir -p {params.outdir} && cd {params.outdir} && \
           singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
           --genomeDir {params.refdir} \
           --readFilesIn {input.R1} {input.R2} \
           --readFilesCommand zcat \
           --outSAMtype BAM SortedByCoordinate \
           --sjdbFileChrStartEnd {input.sjs} \
           --outSAMattrRGline ID:{params.id} \
           --quantMode TranscriptomeSAM GeneCounts && \
           touch {params.outdir}/pass2.OK
           # the filtered and merged SJ sjfilmerged could be use instead the SJ list
           """
# Bams stats used by the multiqc bam
rule bam_stats:
    input:
        bam   = "{outdir}/mapped/starpass2/{{sample}}/Aligned.sortedByCoord.out.bam".format(outdir=config["outdir"])
    output:
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        samtools_bin = config["samtools_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} samtools stats -@ 2 {input.bam} > {output.stats}
        """

# Generate a genes count file for dicoexpress R pipeline
rule merge_count4dicoexpress:
    input:
        lambda wildcards: expand(expand("{outdir}/mapped/starpass2/{sample}/ReadsPerGene.out.tab", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        genecount = "{outdir}/gene_count_dicoexpress.csv".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        genecountlist = "{outdir}/mapped/starpass2/*/ReadsPerGene.out.tab".format(outdir=config["outdir"]),
        bind         = config["BIND"],
        samtools_bin = config["samtools_bin"]
    threads: 1
    shell:
        """
        rm -f {params.outdir}/*.tab {params.outdir}/*.csv

for i in $(ls {params.genecountlist})
do
    sp=$(echo $i|sed -e 's@.*/starpass2/@@g' -e 's@/ReadsPerGene.out.tab@@g')
    #keep the count on the column 4 for Illumina truseq rf stranded RNAseq
    sed 1,4d $i| grep -v '^#'|cut -f1,4 >{params.outdir}/${{sp}}.tab
done

#2)merge all csv 4 a matrix
for i in {params.outdir}/*.tab
do
    base=${{i%%.tab}}
    nue=${{base##*/}} # remove the path
    echo $nue
    sed -i "1iGene_ID\t$nue" $i
done # add column names

N=$(($(ls -l {params.outdir}/*.tab | wc -l)*2)) # count number of files
paste {params.outdir}/*.tab | cut -f 1,$(seq -s, 2 2 $N) > {output.genecount} # merge and keep only one column with gene names

#verif
echo -en "Total genes in original files\t"
grep -vh '^#' {params.outdir}/*.tab|grep -v '^Gene_ID'|cut -f1|sort -u|wc -l
echo -en "Total genes in merged\t"
grep -v '^Gene_ID' {output.genecount}|cut -f1 |sort -u|wc -l
rm -f {params.outdir}/*.tab
        """
