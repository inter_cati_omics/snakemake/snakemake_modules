__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#SNP
#annovar SNP annotation
rule do_qtlseqR:
    input:
        snps = "Bulks/RSp.fb.table",
    output:
        jpg6 = "Bulks/6.Takagi.jpg",
        qtlsT = "Bulks/qtlsT.csv",
        qtlsG = "Bulks/qtlsG.csv",
        filtered = "Bulks/filtered.csv",
        stats = "Bulks/stats.txt"
    params:
        R_bulk_size      = config["R_bulk_size"],
        S_bulk_size      = config["S_bulk_size"],
        nb_takagi_reps      = config["nb_takagi_reps"],
        filter_threshold      = config["filter_threshold"],
        window_size      = config["window_size"],
        false_discovery_rate_G      = config["false_discovery_rate_G"],
        min_depth_in_bulk      = config["min_depth_in_bulk"],
        max_depth_in_bulk      = config["max_depth_in_bulk"]
    shell:
        """
        singularity exec /nosave/project/gafl/tools/containers/R_QTLseqr_v0.7.5.2.sif Rscript scripts/do_qtl.R \
        {input.snps} \
        {output.stats} \
        {output.filtered} \
        {output.qtlsT} \
        {output.qtlsG} \
        {params.R_bulk_size} \
        {params.S_bulk_size} \
        {params.nb_takagi_reps} \
        {params.filter_threshold} \
        {params.window_size} \
        {params.false_discovery_rate_G} \
        {output.jpg6} \
        {params.min_depth_in_bulk} \
        {params.max_depth_in_bulk}
        """
