__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#mapping with bowtie2 SE mode
#reference indexing
#example with and without mark duplicate
#sam 2 bam sorted and statistics
rule bowtie2_index:
    input:
        genome = config["REFPATH"] + "/" + config["GENOME"]
    output:
        config["REFPATH"] + "/" + config["GENOME"] + ".rev.1.bt2",
        config["REFPATH"] + "/" + config["GENOME"] + ".rev.2.bt2",
        config["REFPATH"] + "/" + config["GENOME"] + ".fai"
    params:
        bind         = config["BIND"],
        bwt2_bin      = config["bwt2_bin"],
        samtools_bin = config["samtools_bin"]
    message: "Building Bowtie2 index for reference genome {input.genome}\n"
    shell:
        """
        singularity exec {params.bind} {params.bwt2_bin} bowtie2-build {input.genome} {input.genome}
        singularity exec {params.bind} {params.samtools_bin} samtools faidx {input.genome}
        """

rule bowtie2_se :
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        #fake input used to force index building before alignement if not present
        config["REFPATH"] + "/" + config["GENOME"] + ".rev.1.bt2",
        config["REFPATH"] + "/" + config["GENOME"] + ".rev.2.bt2",
        config["REFPATH"] + "/" + config["GENOME"] + ".fai"
    output:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"]),
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        bwa_bin      = config["bwa_bin"],
        bwt2_bin = config["bwt2_bin"],
        freebayes_bin= config["freebayes_bin"], # freebayes_bin used for sambamba for markdup
        rg           = "@RG\\tID:{sample}\\tSM:{sample}"
    threads: 10
    message: "Mapping reads {input.R1} to {params.idxbase} using bowtie2.\n"
    #converting to bam, sorting and removing dupplicates in a single command!
    shell:
        """
        singularity exec {params.bind} {params.bwt2_bin} bowtie2 \
        --end-to-end \
        --score-min L,-0.7,-0.7 \
        -x {params.idxbase} \
        -1 {input.R1} \
        --threads {threads} --fr -X 600 | \
        | singularity exec {params.bind} {params.samtools_bin} samtools sort -@2 -m 6G -o {output.bam} -
        singularity exec {params.bind} {params.samtools_bin} samtools index -@ 10 {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools flagstat -@ 10 {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools stats -@ 10 {output.bam} >{output.stats}
        #rm -f {params.outdir}/{wildcards.sample}.bam.raw*
        """
