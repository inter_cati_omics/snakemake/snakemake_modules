__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#PREPROC
# adaptor removal, trim PE read with cutadapt
rule cutadapt_pe:
    input:
        R1  = get_fastq1,
        R2  = get_fastq2
        #R1 = lambda wildcards: "../data/"+samples.fq1[wildcards.sample],
        #R2 = lambda wildcards: "../data/"+samples.fq2[wildcards.sample]
        #R1  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna(),
        #R2  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        #R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    message: "Running fastp on files {input.R1} and {input.R2} \n"
    params:
        fqdir         = config["fq_dir"],
        outdir        = config["outdir"],
        modules       = config["MODULES"],
        fastp_bin     = config["fastp_bin"],
        cutadapt_bin  = config["cutadapt_bin"],
        bind          = config["BIND"],
        json          = config["outdir"]+"/fastp/{sample}_trim.json",
        html          = config["outdir"]+"/fastp/{sample}_trim.html",
        tmpout        = config["outdir"]+"/fastp/{sample}",
        primers_for   = config["primers_for"],
        primers_rev   = config["primers_rev"]

    shell:
        """
        singularity exec {params.bind} {params.cutadapt_bin} cutadapt \
        --cores 4 \
        -e 0.10 \
        --action=trim \
        -g file:{params.primers_for} \
        -G file:{params.primers_rev} \
        -o {params.tmpout}.c1.1.fq.gz \
        -p {params.tmpout}.c2.1.fq.gz \
        {params.tmpout}.f1.1.fq.gz \
        {params.tmpout}.f1.2.fq.gz
        rm -f {params.tmpout}.f1.1.fq.gz {params.tmpout}.f1.2.fq.gz
        """
