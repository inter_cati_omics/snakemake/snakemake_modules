__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


# SNP caller
#freebayes parallel rule
rule freebayes_parallel:
    input:
        bam = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
        #on merged bams
        #bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    output:
        var = "{outdir}/variant/{{sample}}_freebayes.vcf".format(outdir=config["outdir"])
        #on merged bams
        #var = "{outdir}/variant/SNPs4all_freebayes.vcf".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        freebayes_bin = config["freebayes_bin"],
        minCoverage = config["parentsMinCoverage"],
        minObservationsToCall = config["parentsMinObservationsToCall"],
        hapLen = "--haplotype-length 0",
        cut_size = 1000000
    threads: 20
    message: "SNPs calling of {input.bam} using freebayes in parallel \n"
    shell:
        """
        zero=$(singularity exec {params.bind} {params.samtools_bin} samtools flagstat {input.bam}|head -n1|sed 's/\s.*//g')
        if [ "$zero" -lt "1" ]
        then
                echo "WARNING {output.var} EMPTY"
                touch {output.var}
                exit 0
        fi
        singularity exec {params.bind} {params.freebayes_bin} \
        freebayes-parallel <( singularity exec {params.bind} {params.freebayes_bin} fasta_generate_regions.py {input.ref_fai} {params.cut_size}) \
        {threads} --genotype-qualities {params.hapLen} \
        -C {params.minObservationsToCall} \
        --min-coverage {params.minCoverage} \
        -f {params.ref} {input.bam} > {output.var}
        exit 0
        """
