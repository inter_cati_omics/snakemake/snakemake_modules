__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#QC
#multiqc for bams
rule multiqc_bam:
    input:
        expand("{outdir}/mapped/{sample}.stats.txt", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"])
        #"{outdir}/multiqc/multiqc_report.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"]+"/mapped/*.stats.txt",
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {input}
        """
