---
title: "AllMine Run Report"
Author: INRAe GAFL 2020"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
params:
    asf     : ""
    awd     : ""
    aannot  : ""
    aref    : ""
    abed    : ""
    asamples: ""
    asnps   : ""
    acov    : ""
output: html_document
---

```{r setup}
knitr::opts_chunk$set(echo = TRUE)
samples = names(params$asamples)
bed     = read.table(params$abed)
snps    = read.table(params$asnps, sep = '\t', header = T)
cov     = read.table(params$acov, sep = '\t')
```

### Your Run in brief
Samples file         : **`r params$asf`** <br/>
Output folder        : **`r params$awd`** <br/>
Reference genome     : **`r tail(strsplit(params$aref, "/"), 1)`** <br/>
Reference annotation : **`r tail(strsplit(params$aannot, "/"), 1)`** <br/>
Reference region     : **`r tail(strsplit(params$aref, "/"), 1)`** <br/>
This run looked for **non synonymous SNPs** in **`r nrow(bed)`** regions and across **`r length(samples)`** samples.
A total of **`r nrow(snps)`** differents SNPs where found.

### SNPs summary
Informations about found SNPs are displayed here.
```{r resume_snp, include= T, echo= T}
#barplot(table(snps$GENE), ylab = "SNPs Count", xlab = "Genes", col = 2:length(snps$GENE), main = "SNPs count per gene", ylim=c(0,table(snps$GENE)+5))
barplot(table(snps$GENE), ylab = "SNPs Count", xlab = "Genes", col = 2:length(snps$GENE), main = "SNPs count per gene")
knitr::kable(snps, caption = "Found SNPs")
```

### Coverage plots

Coverage across regions of interest in your samples are displayed here.
```{r coverage}
for(i in 1:nrow(bed)){
  filtered_cov = subset(cov, subset = V1 == bed$V1[i])
  filtered_cov = subset(filtered_cov, subset = V2 >= bed$V2[i])
  filtered_cov = subset(filtered_cov, subset = V2 <= bed$V3[i])
  plot(filtered_cov[,2], filtered_cov[,3], ylab = "Reading Depth toto",
       xlab = as.character(bed$V1[i]), type = 'l',
       xlim = c(bed[i,2], bed[i,3]), ylim = c(0,max(filtered_cov[,3:ncol(filtered_cov)])+10) )
  for(j in 4:ncol(cov)){
    lines(filtered_cov[,2], filtered_cov[,j], col = j)
  }
  grid()
}

```

### R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

### Session info
```{r info}
sessionInfo()
```

