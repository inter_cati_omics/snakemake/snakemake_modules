__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

# From best gatk practice germilne on multi samples
# https://gatk.broadinstitute.org/hc/en-us/sections/360007226651-Best-Practices-Workflows
# see:
# https://github.com/snakemake-workflows/dna-seq-gatk-variant-calling/blob/master/rules/calling.smk
# https://github.com/gencorefacility/variant-calling-pipeline-gatk4/blob/master/main.nf
# steps
# 1) trim reads
# 2) bwa mem align to genome
# 3) mark duplicates
# 4) use HaplotypeCaller to generate gvcf
# 5) CombineGVCFs
# 6) GenotypeGVCFs on the combined gvcf
# 7) filter your vcf however you want
# 8) You can do base recalibration iteratively now if you want with the filtered vcf.


# SNP caller using GATK4
#gatk4 rules

# 1) for each sample do haplotype caller genomic vcf
rule gatk4_haplotypecaller:
    input:
        bam = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
    output:
        gvcf = "{outdir}/variant/gatk_gvcf/{{sample}}.g.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "SNPs calling in {input.bam} using gatk4 step1 gvcf \n"
    shell:
        """
        zero=$(singularity exec {params.bind} {params.samtools_bin} samtools flagstat {input.bam}|head -n1|sed 's/\s.*//g')
        if [ "$zero" -lt "1" ]
        then
                echo "WARNING {output.var} EMPTY"
                touch {output.gvcf}
                exit 0
        fi
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' HaplotypeCaller \
        --reference {params.ref} \
        --input {input.bam} \
        -ERC GVCF \
        --output {output.gvcf}
        exit 0
        """

# 2) merge all *.g.vcf.gz
rule gatk4_combine_calls:
    input:
        gvcfs = lambda wildcards: expand(expand("{outdir}/variant/gatk_gvcf/{{sample}}.g.vcf.gz", outdir=config["outdir"], sample=samples['SampleName'])),
    output:
        gvcf="{outdir}/variant/gatk_all.g.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 combine all gvcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' CombineGVCFs \
        --variant {input.gvcfs} \
        --reference {params.ref} \
        --output {output.gvcf}
        """

rule gatk4_genotype_variants:
    input:
        gvcf = "{outdir}/variant/gatk_all.g.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 genotype_variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' GenotypeGVCFs \
        --variant {input.gvcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """

# select SNPs only
# for indels:
# -select-type INDEL
rule gatk4_select_snps_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type SNP \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """

# hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# for indels:
# -select-type INDEL
# "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"
rule gatk4_filterSnps:
    input:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.filtered_snps.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf} \
        -filter-name "QD_filter" -filter "QD < 2.0" \
        -filter-name "FS_filter" -filter "FS > 60.0" \
        -filter-name "MQ_filter" -filter "MQ < 40.0" \
        -filter-name "SOR_filter" -filter "SOR > 4.0" \
        -filter-name "MQRankSum_filter" -filter "MQRankSum < -12.5" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum < -8.0"
        """

# select indels only
rule gatk4_select_indels_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select indels (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type INDEL \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """

# hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# for indels:
# "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"
rule gatk4_filterSnps:
    input:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.filtered_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf} \
        -filter-name "QD_filter" -filter "QD < 2.0" \
        -filter-name "FS_filter" -filter "FS > 200.0" \
        -filter-name "MQ_filter" -filter "MQ < 40.0" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum < -20.0"
        """


# recalibration need vcf reference
#rule gatk4_recalibrate_calls:

