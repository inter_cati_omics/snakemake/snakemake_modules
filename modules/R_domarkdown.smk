__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


# Markdown builder
#
#   Starts the .R script for Markdown generation
#
#   Inputs:
#       - Coverage_Track.tab
#       - regions.bed
#       - Non_synonymous_variants_summary.tab
#
#   Output:
#       - Run_report.html
#
#   Parameters:
#       None
rule make_markdown:
    input:
        snps = "{outdir}/Non_synonymous_variants_summary.tab".format(outdir=config["outdir"]),
        cov  = "{outdir}/Coverage_Track.tab".format(outdir=config["outdir"])
    output:
        Rmd  = "{outdir}/Run_report.html".format(outdir=config["outdir"])
    params:
        sf          = config["samplesfile"],
        wd          = "{outdir}".format(outdir=config["outdir"]),
        annot       = config["GENOME_ANNOT"],
        ref         = config["GENOME"],
        regions     = config["REGIONS"],
        inpath      = "{outdir}/variant".format(outdir=config["outdir"]),
        sampleslist = lambda wildcards: expand(expand("{sample}",sample=samples['SampleName'])),
        bind        = config["BIND"],
        allmine_bin = config["allmine_bin"]
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        R -e \
        "rmarkdown::render('./modules/markdown_gen.Rmd',params=list(asf='{params.sf}', awd='{params.wd}',aannot='{params.annot}',aref='{params.ref}', abed ='{params.regions}' ,asamples='{params.sampleslist}',asnps='{input.snps}',acov='{input.cov}'), output_file='{output.Rmd}')"
        rm -rf Run_report_files
        """


