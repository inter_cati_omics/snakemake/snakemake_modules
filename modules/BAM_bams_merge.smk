__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

#rule to merge all bams files
rule merge_bams :
    input:
        # wait for all bams
        bams=lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName'])),
        bamslist = "{outdir}/bams.list".format(outdir=config["outdir"])
        # wait only bams for a given group of samples defined in the sample file
        #bams = lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=getit(wildcards.group))),
    output:
        #bam = "{outdir}/mapped/{{group}}_sorted.bam".format(outdir=config["outdir"])
        bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    params:
        #gr="{group}_sorted.bam",
        #mya=lambda wildcards: getit(wildcards.group),
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"],
        samtools_bin = config["samtools_bin"],
        bind = config["BIND"]
    threads: 10
    shell:"""
        #singularity exec {params.bind} {params.samtools_bin} samtools merge -@ {threads} {output.bam} {input.bams}
        singularity exec {params.bind} {params.samtools_bin} samtools merge -@ {threads} -f -b {input.bamslist} {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools index -@ {threads} {output.bam}
        """

