__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#Generate a list (text file) of bams files
rule bams_list :
    input:
        lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        mylist = "{outdir}/bams.list".format(outdir=config["outdir"])
    params:
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"]
    threads: 1
    shell:"""
        #ls {params.bamsp}/*.bam > {params.outlist}/bams.list
        echo {input}|tr -s '[:space:]' '\n' >{params.outlist}/bams.list
        """

